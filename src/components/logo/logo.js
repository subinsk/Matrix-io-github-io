import React from "react";
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";

const Logo =()=>{
    return(
        <div>
            <Link to ="/" className="navbar-logo">
                <StaticImage 
                    src="https://res.cloudinary.com/dngbmzf6x/image/upload/v1631625656/Matrix.io/logo_ar4gsd.png" alt="Matrix.io" 
                    width={50}
                    height={50}
                />
            </Link>
        </div>
    )
}

export default Logo;