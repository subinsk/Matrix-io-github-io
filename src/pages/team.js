import React from "react";
import {Helmet} from "react-helmet";

import Team from "../components/layouts/team/team";

const TeamPage = () =>{
    return(
        <div>
         <Helmet>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <meta name="description" content="Community" />
          <title>Team | Matrixio</title>
        </Helmet>
            <Team />            
        </div>
    )
}

export default TeamPage;