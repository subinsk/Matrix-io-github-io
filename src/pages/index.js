import * as React from "react";
import {Helmet} from "react-helmet";
import HomePage from "../components/layouts/home/home";

const IndexPage = () => {
  return (
    <>
      <Helmet>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <meta name="description" content="Community" />
          <title>Matrixio</title>
        </Helmet>
      <HomePage /> 
    </>
  )
}

export default IndexPage
